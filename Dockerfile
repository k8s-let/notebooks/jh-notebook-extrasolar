FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-16

USER root

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  batman-package \
  astropy \
  astroquery \
  matplotlib \
  meson-python \
  ninja \
  numpy \
  petitRADTRANS \
  pycosmo \
  PyAstronomy\
  radvel \
  scikit-learn \
  scikit-image \
  scipy  

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install -U --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  jupyterlab-git>=0.50.0

USER 1000
